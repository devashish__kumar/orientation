# Git Summary
Git is a _free and open source_ distributed version control system designed to handle everything from small to very large projects with speed and efficiency. 


**TERMINOLOGY**




**Repository**




It can be termed as Repo.
These are Files/ Fol0ders (code files) that are tracked by GIT.





**GIT Lab**




Storage of GIT Repo.





**Commit**




This is similar as SAVING our work ie. files or codes.
Exist in LOCAL MACHINE unless it is Pushed to Remote Repository.





**Push**





Synching commits to GIT lab.





**Branch**




These are Instances of codes

Master Branch is the main software.





**Merge**




Integrating two Branches
When the branch is Bug free it is merged to Primary codebase(Master Branch).





**Clone**




Copying Repo and paste on Local Machine.⁸





**Fork**




To get a new Repo under your name.





### Branching and Merging
Git allows and encourages you to have multiple local branches that can be entirely independent of each other. The creation, merging, and deletion of those lines of development takes seconds. 
* Frictionless Context Switching - Create a branch to try out an idea, commit a few times, switch back to where you branched from, apply a patch, switch back to where you are experimenting, and merge it in.
* Feature Based Workflow: Create new branches for each new feature you're working on so you can seamlessly switch back and forth between them, then delete each branch when that feature gets merged into your main line.

### Small and Fast
 _Git is fast._ With Git, nearly all operations are performed locally, giving it a huge speed advantage on centralized systems that constantly have to communicate with a server somewhere.

### Distributed
![Shared Repository](https://git-scm.com/images/about/workflow-a@2x.png)
#### Integration Manager Work
Another common Git workflow involves an integration manager — a single person who commits to the 'blessed' repository. A number of developers then clone from that repository, push to their own independent repositories, and ask the integrator to pull in their changes. This is the type of development model often seen with open source or GitHub repositories.

![Integration Manager Work](https://git-scm.com/images/about/workflow-b@2x.png)

#### Staging Area
![Staging Area](https://git-scm.com/images/about/index1@2x.png)



This allows you to stage only portions of a modified file. This feature scales up to as many different changes to your file as needed. 
![Stage](https://git-scm.com/images/about/index2@2x.png)
### Git Commands(Few Important Commands)
```
git add: _add a changed file or a new file to be committed_
git diff: _see the changes between the current version of a file and the version of the file t recently committed_
git commit: _commit changes to the history_
git log: _show the history for a project_
git revert: _undo a change introduced by a specific commit_
git checkout: _switch branches or move within a branch_
git clone: _clone a remote repository_
git pull: _pull changes from a remote repoository_
```
![Code workflow](https://www.google.com/url?sa=i&url=https%3A%2F%2Fmedium.com%2F%40swinkler%2Fgit-workflow-explained-a-step-by-step-guide-83c1c9247f03&psig=AOvVaw16gHw1OTuoeY0mn5fcqbRI&ust=1597806434442000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCOinoK3jo-sCFQAAAAAdAAAAABAD)
