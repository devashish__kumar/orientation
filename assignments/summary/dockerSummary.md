# Docker Engine

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.


### Docker engine is a client-server application with these major components:
* A server which is type of long-running program called a daemon process(the _dockerd_ command).
* A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.

![Basics of Docker](https://docs.docker.com/engine/images/engine-components-flow.png)
The daemon creates and manages Docker _objects_, such as images, containers, networks, and volumes.

### What can I use Docker for?

#### Fast, consistent delivery of your applications
The daemon creates and manages Docker objects, such as images, containers, networks, and volumes.
Consider the following example scenario:
* Your developers write code locally and share their work with their colleagues using Docker containers.
* They use Docker to push their applications into a test environment and execute automated and manual tests.
* When developers find bugs, they can fix them in the development environment and redeploy them to the test environment for testing and validation.


#### Responsive deployment and scaling
ocker’s container-based platform allows for highly portable workloads. Docker containers can run on a developer’s local laptop, on physical or virtual machines in a data center, on cloud providers, or in a mixture of environments.

### Docker architechture
![Basics of Docker](https://docs.docker.com/engine/images/architecture.svg)


#### The Docker Client 
The Docker client (`docker`) is the primary way that many Docker users interact with Docker. When you use commands such as `docker run`, the client sends these commands to `dockerd`, which carries them out. The docker command uses the Docker API. The Docker client can communicate with more than one daemon.
#### Docker Registries
A Docker _registry_  stores Docker images. Docker Hub is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default. You can even run your own private registry.

When you use the `docker pull` or `docker run` commands, the required images are pulled from your configured registry. When you use the `docker push` command, your image is pushed to your configured registry.
\


#### Images
Docker Image is a package of all the tools one would need to run applications.
Image contains requirements to run application and is deployed to Docker as container

Requirements:
- code 
- runtime
- libraries
- env variables
- config files.

#### Containers
Each have a specific job, their own operating system, their own isolated cpu
processes, memory and cpu resources.
- Running docker image
- Can create multiple containers
![pic4](/uploads/9c3a292e8e3895bf850add607209da3b/pic4.png)

**Working of Container**

- Containers are instance of images ie use them to create an enviroment and run applications.
- Containers are the execution part of docker analogues to "process"

#### Few Important Docker Commands
```
* Usage: docker pull <image name> 
* Usage: docker run -it -d <image name> 
* Usage: docker exec -it <container id> bash
* Usage: docker stop <container id>
* Usage: docker kill <container id>
* Usage: docker commit <conatainer id> <username/imagename>
* Usage: docker push <username/image name>
* Usage: docker rm <container id>
* Usage: docker rmi <container id>
```






